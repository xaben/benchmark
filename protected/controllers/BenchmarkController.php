<?php

class BenchmarkController extends Controller
{
	/**
	 * Fetch a particular model.
	 */
	public function actionOne()
	{
        $id = 1;
        $startMem = memory_get_usage();
        $stopwatch = new \Symfony\Component\Stopwatch\Stopwatch();
        $stopwatch->start('foo');

        $model=NewsNews::model()->findByPk($id);

        $event = $stopwatch->stop('foo');

        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        echo "Time diff: $time ms \n";
        echo "Memory diff: $memory kB \n";
	}

    /**
     * Fetch all models.
     */
    public function actionAll()
    {
        $startMem = memory_get_usage();
        $stopwatch = new \Symfony\Component\Stopwatch\Stopwatch();
        $stopwatch->start('foo');

        $models = NewsNews::model()->findAll();


        $event = $stopwatch->stop('foo');
        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        echo "Time diff: $time ms \n";
        echo "Memory diff: $memory kB \n";
    }

    /**
     * Fetch all categories with all joins.
     */
    public function actionJoin()
    {
        $startMem = memory_get_usage();
        $stopwatch = new \Symfony\Component\Stopwatch\Stopwatch();
        $stopwatch->start('foo');

        $models = NewsNews::model()->with('category', 'newsTags')->findAll();

        $event = $stopwatch->stop('foo');
        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        echo "Time diff: $time ms \n";
        echo "Memory diff: $memory kB \n";
    }
}
