<?php

class DoctrineObjectController extends Controller
{
	/**
	 * Displays a particular model.
	 */
	public function actionOne()
	{
        $id = 1;
        $startMem = memory_get_usage();
        $stopwatch = new \Symfony\Component\Stopwatch\Stopwatch();
        $stopwatch->start('foo');

        $models = $this->getEntityManager()->getRepository('\Application\Entity\News')->getOne($id, 'object');

        $event = $stopwatch->stop('foo');

        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        echo "Time diff: $time ms \n";
        echo "Memory diff: $memory kB \n";
	}

    /**
     * Displays a particular model.
     */
    public function actionAll()
    {
        $startMem = memory_get_usage();
        $stopwatch = new \Symfony\Component\Stopwatch\Stopwatch();
        $stopwatch->start('foo');

        $models = $this->getEntityManager()->getRepository('\Application\Entity\News')->getAll('object');

        $event = $stopwatch->stop('foo');
        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        echo "Time diff: $time ms \n";
        echo "Memory diff: $memory kB \n";
    }

    /**
     * Fetch all categories with all joins.
     */
    public function actionJoin()
    {
        $startMem = memory_get_usage();
        $stopwatch = new \Symfony\Component\Stopwatch\Stopwatch();
        $stopwatch->start('foo');

        $models = $this->getEntityManager()->getRepository('\Application\Entity\News')->getCategories('object');

        $event = $stopwatch->stop('foo');
        $time = $event->getDuration();
        $endMem = memory_get_usage();
        $memory = round(($endMem - $startMem)/1024);

        echo "Time diff: $time ms \n";
        echo "Memory diff: $memory kB \n";
    }
}
