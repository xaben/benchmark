<?php

// change the following paths if necessary
$yii = __DIR__ .'/../../vendor/yiisoft/yii/framework/yii.php';
$config = __DIR__ .'/../config/console.php';

$composer_autoload=dirname(__FILE__).'/../../vendor/autoload.php';
/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require($composer_autoload);
$loader->addPsr4('Application\\', dirname(__FILE__).'/../protected');

require_once($yii);
Yii::createWebApplication($config);
Yii::setPathOfAlias('Symfony', Yii::getPathOfAlias('application.vendor.Symfony'));

$em = Yii::app()->doctrine->getEntityManager();
$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));

\Doctrine\ORM\Tools\Console\ConsoleRunner::run($helperSet);