<?php

return [
        'class' => 'DoctrineComponent',
        'basePath' => __DIR__ . '/../',
        'proxyPath' => __DIR__ . '/../proxies',
        'entityPath' => array(
            __DIR__ . '/../Entity'
        ),
        'driver' => 'pdo_mysql',
        'user' => 'root',
        'password' => 'root',
        'host' => '127.0.0.1',
        'dbname' => 'book'
];