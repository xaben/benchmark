Benchmark Yii AR vs Doctrine 2
==============================

Single entity with no JOINS
-------------
### Yii ActiveRecord to Object

Time diff: 3 ms Memory diff: 685 kB 

### Doctrine2 to Object

Time diff: 7 ms Memory diff: 1711 kB 

### Doctrine2 to Array

Time diff: 6 ms Memory diff: 1536 kB

### Symfony Doctrine2 to Object

Time diff: 3 ms Memory diff: 756 kB 

### Symfony Doctrine2 to Array

Time diff: 2 ms Memory diff: 572 kB 
 
All entities with no JOINS
-------------
### Yii ActiveRecord to Object
 
Loaded: 711 models. Time diff: 22 ms Memory diff: 3096 kB  
 
### Doctrine2 to Object
 
Loaded: 711 models. Time diff: 69 ms Memory diff: 5097 kB 
 
### Doctrine2 to Array
 
Loaded: 711 models. Time diff: 47 ms Memory diff: 3349 kB 
 
### Symfony Doctrine2 to Object
   
Time diff: 56 ms Memory diff: 4190 kB
   
### Symfony Doctrine2 to Array
   
Time diff: 28 ms Memory diff: 2415 kB 
 
 
JOIN news with related category and tags (load 711)
-------------
### Yii ActiveRecord to Object

Loaded: 711 models. Time diff: 105 ms Memory diff: 5307 kB  

### Doctrine2 to Object

Loaded: 711 models. Time diff: 268 ms Memory diff: 7803 kB 

### Doctrine2 to Array

Loaded: 711 models. Time diff: 194 ms Memory diff: 6023 kB  
 
### Symfony Doctrine2 to Object
   
Time diff: 236 ms Memory diff: 6869 kB 
   
### Symfony Doctrine2 to Array
   
Time diff: 160 ms Memory diff: 5089 kB 
 
Resorces
--------

 * http://culttt.com/2014/07/07/doctrine-2-different-eloquent/
 * http://weavora.com/blog/2013/03/26/why-we-prefer-symfony2-over-yii-framework/
 * http://kore-nordmann.de/blog/why_active_record_sucks.html
 * https://toster.ru/q/16887
 * http://russellscottwalker.blogspot.com/2013/10/active-record-vs-data-mapper.html